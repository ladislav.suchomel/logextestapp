export const environment = {
  production: true
};

export const firebaseConfig = {
  apiKey: "AIzaSyA1Rq_sahhx9x7rfNygvEZpEiHid19hZmI",
  authDomain: "logextestapp.firebaseapp.com",
  databaseURL: "https://logextestapp.firebaseio.com",
  projectId: "logextestapp",
  storageBucket: "logextestapp.appspot.com",
  messagingSenderId: "831849202944",
  appId: "1:831849202944:web:03a06f6a091ce38b755fe5",
  measurementId: "G-9PZRB6PQKF"
};

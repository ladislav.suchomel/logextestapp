// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyA1Rq_sahhx9x7rfNygvEZpEiHid19hZmI",
  authDomain: "logextestapp.firebaseapp.com",
  databaseURL: "https://logextestapp.firebaseio.com",
  projectId: "logextestapp",
  storageBucket: "logextestapp.appspot.com",
  messagingSenderId: "831849202944",
  appId: "1:831849202944:web:03a06f6a091ce38b755fe5",
  measurementId: "G-9PZRB6PQKF"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

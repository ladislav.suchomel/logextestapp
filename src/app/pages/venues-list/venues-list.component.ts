import {
  AfterViewInit,
  Component,
  ComponentFactory,
  ComponentFactoryResolver, ElementRef, OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {DataService} from "../../services/data.service";
import {FilterModalComponent} from "../../shared/components/filter-modal/filter-modal.component";
import {TableSettingsService} from "../../shared/utils/table-utils/table-settings.service";
import {debounceTime, filter, take} from "rxjs/operators";
import {MapService} from "../../services/map.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-venues-list',
  templateUrl: 'venues-list.component.html',
  styleUrls: ['venues-list.component.scss'],
  providers: [
    TableSettingsService
  ]
})

export class VenuesListComponent implements OnDestroy, OnInit{

  @ViewChild("filterRoot",{ read: ViewContainerRef }) filterRoot:ViewContainerRef;
  @ViewChild('mapContainer') gmap: ElementRef;

  private dataUpdatedSub:Subscription;
  public map: google.maps.Map;
  public isMapActive:boolean;

  constructor(public dataService:DataService,
              private resolver: ComponentFactoryResolver,
              public mapService:MapService,
              public tableSettingsService:TableSettingsService ) {
  }

  ngOnInit() {
    this.dataUpdatedSub = this.tableSettingsService.dataUpdated$.pipe(debounceTime(600)).subscribe(data => {
      if(!this.map)
        this.map = this.mapService.mapInitializer(this.gmap, this.mapService.DEFAULT_LIST_MAP_OPTIONS);

      this.mapService.updateMarkers(data, this.map);
    });
  }

  public openFilter() {
    const factory: ComponentFactory<any>= this.resolver.resolveComponentFactory(FilterModalComponent);
    let filterComponent = this.filterRoot.createComponent(factory);
    filterComponent.instance.citiesList = this.getUniqueCities();
    filterComponent.instance.yearsList = this.getUniqueYears();
    filterComponent.instance.onModalClose.pipe(take(1)).subscribe(() => {this.closeFilter();});
  }

  public closeFilter() {
    this.filterRoot.clear();
  }

  private getUniqueCities() {
    let _citiesArray = [];

    this.dataService.primaryDataSet.forEach(dataEntry => {
      _citiesArray.push(dataEntry.location.city);
    });

    return [...new Set(_citiesArray)].sort();
  }

  private getUniqueYears() {
    let _yearsArray = [];

    this.dataService.primaryDataSet.forEach(dataEntry => {
      if(dataEntry.dates.startdate) {
        _yearsArray.push(dataEntry.dates.startdate.split('-')[0]);
      }
    });

    return [...new Set(_yearsArray)].sort();
  }

  public toggleMap() {
    if(!this.isMapActive) {
      this.isMapActive = true;
      this.map = this.mapService.mapInitializer(this.gmap, this.mapService.DEFAULT_LIST_MAP_OPTIONS);
    } else {
      this.isMapActive = false;
      this.map = this.mapService.disposeMap();
    }
  }

  ngOnDestroy() {
    this.dataUpdatedSub.unsubscribe();
    this.map = this.mapService.disposeMap();
  }
}

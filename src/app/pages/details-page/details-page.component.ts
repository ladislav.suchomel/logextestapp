import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ElementRef,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {VenueLanguage, GenericDataModel} from "../../models/genericDataModel";
import {DataService} from "../../services/data.service";
import {TableSettingsService} from "../../shared/utils/table-utils/table-settings.service";
import {MapService} from "../../services/map.service";
import {Subscription} from "rxjs";
import {debounceTime, take} from "rxjs/operators";
import {FilterModalComponent} from "../../shared/components/filter-modal/filter-modal.component";
import {DetailsFilterModal} from "../../shared/components/details-filter-modal/details-filter-modal.component";

@Component({
  selector: 'app-details-page',
  templateUrl: 'details-page.component.html',
  styleUrls: ['details-page.component.scss'],
  providers: [
    TableSettingsService
  ]
})

export class DetailsPageComponent implements OnInit{

  private id:string;
  public venue:GenericDataModel;
  public activeLanguage:VenueLanguage = 'nl';
  public isMapActive:boolean;

  @ViewChild("filterRoot",{ read: ViewContainerRef }) filterRoot:ViewContainerRef;
  @ViewChild('mapContainer') gmap: ElementRef;

  private dataUpdatedSub:Subscription;
  public map: google.maps.Map;
  public dataSet:Array<GenericDataModel>;

  private lat:number;
  private lng:number;

  constructor(private activatedRoute:ActivatedRoute,
              private router:Router,
              private dataService:DataService,
              public mapService: MapService,
              private resolver: ComponentFactoryResolver,
              public tableSettingsService: TableSettingsService) {
  }

  ngOnInit(){
    this.id = this.activatedRoute.snapshot.queryParams['id'];
    this.venue = this.dataService.primaryDataSet.find(dataEntry => {return dataEntry.trcid == this.id});

    this.lat = this.mapService.getLatLng(this.venue.location.latitude);
    this.lng =this.mapService.getLatLng(this.venue.location.longitude);

    this.tableSettingsService.setDetailsDataForm();

    this.dataUpdatedSub = this.tableSettingsService.dataUpdated$.pipe(debounceTime(600)).subscribe(data => {
      if(!this.map) {
        this.map = this.mapService.mapInitializer(this.gmap, Object.assign({center: new google.maps.LatLng(this.lat, this.lng)}, this.mapService.DEFAULT_DETAIL_MAP_OPTIONS), true);
      }

      this.mapService.updateMarkers(data, this.map);
    });

    this.dataSet = this.mapService.getCloseEvents(this.dataService.eventsDataSet,
      new google.maps.LatLng(this.mapService.getLatLng(this.venue.location.latitude),
        this.mapService.getLatLng(this.venue.location.longitude)),
      1);
  }

  public navigateBack() {
    this.router.navigate(['./..'], {relativeTo: this.activatedRoute});
  }

  public toggleMap() {
    if(!this.isMapActive) {
      this.isMapActive = true;

      this.map = this.mapService.mapInitializer(this.gmap, Object.assign({center: new google.maps.LatLng(this.lat, this.lng)}, this.mapService.DEFAULT_DETAIL_MAP_OPTIONS), true);
    } else {
      this.isMapActive = false;
      this.map = this.mapService.disposeMap();
    }
  }

  public openFilter() {
    const factory: ComponentFactory<any>= this.resolver.resolveComponentFactory(DetailsFilterModal);
    let filterComponent = this.filterRoot.createComponent(factory);
    filterComponent.instance.onModalClose.pipe(take(1)).subscribe(() => {this.closeFilter();});
  }

  public closeFilter() {
    this.filterRoot.clear();
  }
}

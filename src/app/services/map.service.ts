import {ElementRef, Injectable} from "@angular/core";
import {GenericDataModel} from "../models/genericDataModel";

@Injectable()
export class MapService {
  private _currentMarkers:Array<google.maps.Marker> = [];
  private _editPageActive:boolean;

  public DEFAULT_LIST_MAP_OPTIONS: google.maps.MapOptions = {
    center: new google.maps.LatLng(52.377956, 4.897070),
    zoom: 8,
    fullscreenControl: false
  };

  public DEFAULT_DETAIL_MAP_OPTIONS: google.maps.MapOptions = {
    zoom: 13,
    fullscreenControl: false
  };

  public mapInitializer(gmap: ElementRef,
                        mapOptions: google.maps.MapOptions,
                        _editPageActive?:boolean) {

    this._editPageActive = _editPageActive;
    return new google.maps.Map(gmap.nativeElement,
      mapOptions);
  }

  public disposeMap() {
    return null;
  }

  public updateMarkers(newData, map: google.maps.Map) {
    if (newData.length == 0)
      return;

    this._currentMarkers.forEach(marker => {
      marker.setMap(null);
    });

    let bounds = new google.maps.LatLngBounds();

    newData.forEach(venue => {
      let marker = new google.maps.Marker();
      let markerLatLng = new google.maps.LatLng(this.getLatLng(venue.location.latitude), this.getLatLng(venue.location.longitude))

      if(!this._editPageActive) {
        let infoWindow = new google.maps.InfoWindow({content: this.getContentString(venue)});

        marker.addListener('click', () => {
          marker.setAnimation(google.maps.Animation.BOUNCE);
          setTimeout(() => {marker.setAnimation(null)}, 1);

          infoWindow.open(map, marker);
        });
      } else {
        marker.setIcon('../../../assets/img/blue-dot.png');
      }

      marker.setPosition(markerLatLng);
      marker.setMap(map);
      bounds.extend(markerLatLng);
      marker.setAnimation(null);

      this._currentMarkers.push(marker);
    });

    map.fitBounds(bounds);
  }

  public getLatLng(value:string):number {
    return Number(value.replace(',', '.'));
  }

  // #source:https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
  public getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  public deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  public getCloseEvents(dataSet:Array<GenericDataModel>, compareTo:google.maps.LatLng, distance:number):Array<GenericDataModel> {

    return dataSet.filter(dataEntry =>{
      if(this.getDistanceFromLatLonInKm(this.getLatLng(dataEntry.location.latitude),
        this.getLatLng(dataEntry.location.longitude),
        compareTo.lat(),
        compareTo.lng()) <= 1)
        return dataEntry;
    })
  }

  private getContentString(venue) {
    return '<a href="./venue-detail?id=' + venue.trcid + '" target="_blank">'+ venue.title +'</a>';
  }
}

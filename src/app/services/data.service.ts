import {Injectable} from "@angular/core";
import {GenericDataModel} from "../models/genericDataModel";
import Data = google.maps.Data;

@Injectable()
export class DataService {

  private _primaryDataSet: Array<GenericDataModel>;
  private _eventsDataSet: Array<GenericDataModel>;

  public get primaryDataSet():Array<GenericDataModel> {
    return this._primaryDataSet;
  }

  public get eventsDataSet(): Array<GenericDataModel> {
    return this._eventsDataSet;
  }

  constructor() {
    this.initData();
  }

  private async initData() {
    const primaryDataSetResponse = await fetch('https://firebasestorage.googleapis.com/v0/b/logextestapp.appspot.com/o/venues.dataset.ts?alt=media&token=c0213471-0b41-4deb-9722-815426d8a01a');
    const eventDataSetResponse = await fetch('https://firebasestorage.googleapis.com/v0/b/logextestapp.appspot.com/o/events.dataset.ts?alt=media&token=cea8a929-aabf-4b72-a86e-6b5dc964eeca');
    this._primaryDataSet = await primaryDataSetResponse.json();
    this._eventsDataSet = await eventDataSetResponse.json();
    this.mapDataset(this._primaryDataSet);
    this.mapDataset(this._eventsDataSet);
  }
  private mapDataset(dataSet):void {
    dataSet.forEach(dataSetEntry => {
      if(dataSetEntry.dates.startdate)
        dataSetEntry.dates.startdate = DataService.convertDate(dataSetEntry.dates.startdate);

      if(dataSetEntry.dates.enddate)
        dataSetEntry.dates.enddate = DataService.convertDate(dataSetEntry.dates.enddate);

      if(dataSetEntry.dates.singles)
        dataSetEntry.dates.singles.forEach(singleDate => {
          dataSetEntry.dates.singles[dataSetEntry.dates.singles.indexOf(singleDate)] = DataService.convertDate(singleDate);
        });

      if(dataSetEntry.lastupdated)
        dataSetEntry.lastupdated = DataService.convertDate(dataSetEntry.lastupdated);
    });
  }

  private static convertDate(originalDate:string):string {
    return originalDate.split('-').reverse().join('-');
  }
}

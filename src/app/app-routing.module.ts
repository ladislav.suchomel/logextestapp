import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VenuesListComponent} from "./pages/venues-list/venues-list.component";
import {DetailsPageComponent} from "./pages/details-page/details-page.component";


const routes: Routes = [{
  path: 'venues-list',
  component: VenuesListComponent
}, {
  path: 'venue-detail',
  component: DetailsPageComponent
},{
  path: '**',
  redirectTo: 'venues-list',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

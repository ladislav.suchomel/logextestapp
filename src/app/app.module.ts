import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {VenuesListComponent} from "./pages/venues-list/venues-list.component";
import {AngularFireModule} from "@angular/fire";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {firebaseConfig} from "../environments/environment";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {SharedModule} from "./shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {DetailsPageComponent} from "./pages/details-page/details-page.component";
import {MapService} from "./services/map.service";
import {CommonModule} from "@angular/common";
import {NavigationEnd, Route, Router} from "@angular/router";
import {DataService} from "./services/data.service";

@NgModule({
  declarations: [
    AppComponent,
    VenuesListComponent,
    DetailsPageComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    ScrollingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [
    DataService,
    MapService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
      }
    });
  }
}

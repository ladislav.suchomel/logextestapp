import {Component, Input} from "@angular/core";
import {DEFAULT_YEARS_ARRAY} from "../../shared.variables";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'date-select-component',
  templateUrl: 'date-select.component.html',
  styleUrls: ['date-select.component.scss']
})

export class DateSelectComponent {
  public months:Array<number> = DEFAULT_YEARS_ARRAY;
  public currentYearStart:number = new Date().getFullYear();
  public currentYearEnd:number = new Date().getFullYear();

  public selectedStartMonth:number;
  public selectedEndMonth:number;

  @Input() startDateFormControl:FormControl;
  @Input() endDateFormControl:FormControl;

  public setStartMonth(month:number) {
    this.selectedStartMonth = month;
    this.setFormValue();
  }

  public setEndMonth(month:number) {
    this.selectedEndMonth = month;
    this.setFormValue();
  }

  private setFormValue() {
    if(this.selectedStartMonth && this.selectedEndMonth) {
      this.startDateFormControl.setValue([this.currentYearStart, this.selectedStartMonth, 1].join('.'));
      this.endDateFormControl.setValue([this.currentYearEnd, this.selectedEndMonth, 1].join('.'));
    }
  }
}

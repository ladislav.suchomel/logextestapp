import {Component, EventEmitter, HostBinding, Input, Output} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {TableSettingsService} from "../../utils/table-utils/table-settings.service";
import {FormArray, FormControl} from "@angular/forms";
import {STANDARD_ANIM} from "../../shared.animations";

@Component({
  selector: 'app-filter-modal',
  templateUrl: 'filter-modal.component.html',
  styleUrls: ['filter-modal.component.scss'],
  host: {
    '[@appear]': 'true'
  },
  animations: STANDARD_ANIM
})

export class FilterModalComponent {
  @HostBinding('class.standard-modal') true;
  @Output() onModalClose = new EventEmitter();
  @Input() citiesList: Array<string>;
  @Input() yearsList: Array<string>;

  constructor(public tableSettingsService:TableSettingsService) {

  }

  public yearCheckboxChanged(value) {
    if(this.tableSettingsService.tableDataForm.value.years.indexOf(value) == -1)
      (this.tableSettingsService.tableDataForm.get('years') as FormArray)
        .push(new FormControl(value));
    else
      (this.tableSettingsService.tableDataForm.get('years') as FormArray)
        .removeAt(this.tableSettingsService.tableDataForm.value.years.indexOf([value]));
  }
}

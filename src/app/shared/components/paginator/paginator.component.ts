import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {TableSettingsService} from "../../utils/table-utils/table-settings.service";
import {log} from "util";

@Component({
  selector: 'app-paginator',
  templateUrl: 'paginator.component.html',
  styleUrls: ['paginator.component.scss']
})

export class PaginatorComponent implements OnChanges{

  @Input() dataSetSize:number;

  constructor(public tableSettingsService:TableSettingsService) {

  }

  ngOnChanges(changes: SimpleChanges) {
  }

  public getPaginatorBoxesCount():Array<number> {
    let pagesCount = Math.ceil(this.dataSetSize / this.tableSettingsService.pageSize);

    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
      pages.push(i);
    }
    return pages;
  }

  public canIncreasePageNo() {
    return this.getPaginatorBoxesCount().length > this.tableSettingsService.activePage;
  }

  public increasePageNo() {
    this.tableSettingsService.activePage++;
  }

  public canDecreasePageNo() {
    return this.tableSettingsService.activePage != 1;
  }

  public decreasePageNo() {
    this.tableSettingsService.activePage--;
  }
}

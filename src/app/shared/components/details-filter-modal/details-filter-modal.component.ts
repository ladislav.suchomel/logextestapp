import {Component, EventEmitter, HostBinding, Output} from "@angular/core";
import {STANDARD_ANIM} from "../../shared.animations";
import {TableSettingsService} from "../../utils/table-utils/table-settings.service";

@Component({
  selector: 'details-filter-page',
  templateUrl: 'details-filter-modal.component.html',
  styleUrls: ['details-filter-modal.component.scss'],
  host: {
    '[@appear]': 'true'
  },
  animations: STANDARD_ANIM
})
export class DetailsFilterModal {
  @HostBinding('class.standard-modal') true;
  @Output() onModalClose = new EventEmitter();

  constructor(public tableSettingsService:TableSettingsService) {
  }
}

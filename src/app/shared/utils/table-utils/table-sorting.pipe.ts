import {Input, Pipe, PipeTransform} from "@angular/core";
import {TableSettingsService} from "./table-settings.service";
import {SortingModel} from "../../../models/sorting.model";
import {MapService} from "../../../services/map.service";
import {GenericDataModel} from "../../../models/genericDataModel";

@Pipe({
  name: 'TableSortingPipe',
  pure: true
})

export class TableSortingPipe implements PipeTransform{

  constructor(private tableSettingsService: TableSettingsService,
              private mapService: MapService) {

  }

  transform(value: Array<GenericDataModel>, config:any, pageNo:number, sortBy:string, sortOrder:string, mapActive:boolean): Array<any> {
    if(this.tableSettingsService.sortBy) {
      value = this.sortByOrder(value)
    }

    value = this.sortByFilters(value, config);
    this.tableSettingsService.currentDataSetLength = value.length;
    value = this.sortByPageNumber(value);


    if(mapActive)
      this.tableSettingsService.dataUpdated$.next(value);

    return value;
  }

  private sortByFilters(value, config) {
    if(config.venueName)
      value = value.filter(item => {return item.title.indexOf(config.venueName) > -1});
    if(config.cityName)
      value = value.filter(item => {return item.location.city == config.cityName});
    if(config.postcode)
      value = value.filter(item => {return item.location.zipcode.indexOf(config.postcode) > -1});
    if(config.years && config.years.length > 0)
      value = value.filter(item => {
        if(item.dates.startdate) {
          return config.years.indexOf(item.dates.startdate. split('-')[0]) > -1;
        }
      });
    if(config.startDate && config.endDate) {
      value = value.filter(item => {
        if(item.dates.startdate && item.dates.enddate) {
          let eventStartDate = new Date(item.dates.startdate);
          let eventEndDate = new Date(item.dates.enddate);
          let configStartDate = new Date(config.startDate);
          let configEndDate = new Date(config.endDate);

          return (eventStartDate >= configStartDate && eventStartDate <= configEndDate) &&
            (eventEndDate <= configEndDate && eventEndDate >= configStartDate);
        }

        if(item.dates.singles) {
          return item.dates.singles.filter(item => {
            let itemDate = new Date(item);
            let configStartDate = new Date(config.startDate);
            let configEndDate = new Date(config.endDate);

            return itemDate >= configStartDate && itemDate <= configEndDate
          }).length > 0;
        }

      })
    }

    return value;
  }

  private sortByPageNumber(value) {
    let activePage:number = this.tableSettingsService.activePage;
    let pageSize:number = this.tableSettingsService.pageSize;

    return value.slice((activePage - 1) * pageSize, activePage * pageSize);
  }

  private sortByOrder(value) {
    let sortBy = this.tableSettingsService.sortBy;
    let sortOrder:SortingModel = this.tableSettingsService.sortOrder;

    let prop = sortBy.split('.');
    let len = prop.length;

    return value.sort(function (a, b) {
      let i = 0;
      while( i < len ) { a = a[prop[i]]; b = b[prop[i]]; i++; }

      if(sortOrder === 'ASC')
        return a.toString().toUpperCase() > b.toString().toUpperCase() ? 1 : -1;
      else
        return a > b ? -1 : 1;
    });
  }
}

import {EventEmitter, Injectable} from "@angular/core";
import {SortingModel} from "../../../models/sorting.model";
import {FormBuilder, FormGroup} from "@angular/forms";

@Injectable()
export class TableSettingsService {

  private _PAGE_SIZE: number = 25;
  private _activePage: number = 1;
  private _sortBy: string;
  private _sortOrder: SortingModel = 'ASC';

  public sortingUpdated = new EventEmitter();

  public tableDataForm:FormGroup;
  public detailsDataForm:FormGroup;

  public currentDataSetLength:number;
  public dataUpdated$ = new EventEmitter();

  constructor(private formBuilder:FormBuilder) {
    this.initMainForm();
  }

  public get pageSize():number {
    return this._PAGE_SIZE;
  }

  public get activePage():number {
    return this._activePage;
  }

  public set activePage(pageNum:number){
    this._activePage = pageNum;
  }

  public get sortBy() {
    return this._sortBy;
  }

  public set sortBy(sortBy:string) {
    this._sortBy = sortBy;
    this._activePage = 1;
    this.sortingUpdated.emit();
  }

  public set sortOrder(sortOrder:SortingModel) {
    this._sortOrder = sortOrder;
    this.sortingUpdated.emit();
  }

  public get sortOrder():SortingModel {
    return this._sortOrder;
  }

  public switchSortOrder() {
    if(this.sortOrder == 'ASC')
      this.sortOrder = 'DESC';
    else
      this.sortOrder = 'ASC';
  }

  private initMainForm() {
    this.tableDataForm = this.formBuilder.group({
      'venueName': [],
      'cityName': [],
      'postcode': [],
      'years': this.formBuilder.array([])
    });

    this.tableDataForm.valueChanges.subscribe(() => {this._activePage = 1;});
  }

  public setDetailsDataForm() {
    this.detailsDataForm = this.formBuilder.group({
      'venueName': [],
      'startDate': [],
      'endDate': []
    });

    this.detailsDataForm.valueChanges.subscribe(() => {this._activePage = 1;});
  }
}

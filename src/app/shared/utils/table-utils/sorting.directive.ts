import {Directive, HostBinding, HostListener, Input, OnDestroy} from "@angular/core";
import {TableSettingsService} from "./table-settings.service";
import {Subscription} from "rxjs";

@Directive({
  selector: '[table-sort]'
})

export class SortingDirective implements OnDestroy{

  @Input() sortBy:string;
  @HostBinding('class.cell-sortable') _isSortable:boolean = true;
  @HostBinding('class.cell-sorting-active') _isActive:boolean;
  @HostBinding('class.cell-sorting-desc-active') _sortingDesc:boolean;

  private sortingUpdatedSubscription:Subscription;

  constructor(private tableSettingsService:TableSettingsService) {
    this.sortingUpdatedSubscription = this.tableSettingsService.sortingUpdated.subscribe(() => this.setSorting())
  }

  @HostListener('click')
  onCellClicked() {
    if(this.tableSettingsService.sortBy == this.sortBy)
      this.tableSettingsService.switchSortOrder();

    this.tableSettingsService.sortBy = this.sortBy;
  }

  private setSorting() {
    if(this.tableSettingsService.sortBy == this.sortBy)
      this._isActive = true;
    else {
      this._isActive = false;
      return;
    }

    if(this.tableSettingsService.sortOrder == 'DESC')
      this._sortingDesc = true;
    else
      this._sortingDesc = false;
  }

  ngOnDestroy(){
    this.sortingUpdatedSubscription.unsubscribe();
  }
}

import {NgModule} from "@angular/core";
import {TransformUppercasePipe} from "./utils/transform-uppercase.pipe";
import {TableSettingsService} from "./utils/table-utils/table-settings.service";
import {PaginatorComponent} from "./components/paginator/paginator.component";
import {CommonModule} from "@angular/common";
import {SortingDirective} from "./utils/table-utils/sorting.directive";
import {FilterModalComponent} from "./components/filter-modal/filter-modal.component";
import {ReactiveFormsModule} from "@angular/forms";
import {TableSortingPipe} from "./utils/table-utils/table-sorting.pipe";
import {DetailsFilterModal} from "./components/details-filter-modal/details-filter-modal.component";
import {DateSelectComponent} from "./components/date-select-component/date-select.component";

@NgModule({
  declarations: [
    TransformUppercasePipe,
    TableSortingPipe,
    PaginatorComponent,
    SortingDirective,
    FilterModalComponent,
    DetailsFilterModal,
    DateSelectComponent
  ], entryComponents: [
    FilterModalComponent,
    DetailsFilterModal
  ], exports: [
    TransformUppercasePipe,
    TableSortingPipe,
    PaginatorComponent,
    SortingDirective,
    FilterModalComponent,
    DetailsFilterModal,
    DateSelectComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
})

export class SharedModule {
}

import {animate, state, style, transition, trigger} from "@angular/animations";

export const STANDARD_ANIM = [
  trigger('appear', [
    state('*', style({opacity: 1})),
    state('void', style({opacity: 0})),
    transition('void => *', [
      animate(100)
    ]),
    transition('* => void', [
      animate(100)
    ])
  ])
]

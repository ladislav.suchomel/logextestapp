export interface GenericDataModel {
  trcid: string;
  title: string;
  details: {
    de: VenueDetails,
    en: VenueDetails,
    fr: VenueDetails,
    nl: VenueDetails,
    it: VenueDetails,
    es: VenueDetails,
  },
  types: Array<VenueType>,
  location: VenueLocation,
  urls: Array<string>,
  media: Array<VenueMedia>,
  dates: {
    startdate?:string,
    enddate?:string
    singles?:Array<string>
  },
  lastupdated: string
}

export interface VenueDetails {
  language: string,
  title: string,
  calendarsummary: string,
  shortdescription:string,
  longdescription:string
}

export interface VenueLocation {
  name: string,
  city: string,
  adress: string,
  zipcode: string,
  latitude: string,
  longitude: string
}

export interface VenueType {
  type:string,
  catid:string
}

export interface VenueMedia {
  url: string,
  main: boolean | string
}

export type VenueLanguage = "de" | "en" | "fr" | "nl" | "it" | "es";
